"""
Purpose: 
Playing with hash tables. I'm using the following reference :
https://leetcode.com/explore/learn/card/hash-table

Author: Mike Moore
Date: May 19, 2021
"""
from collections import namedtuple

class HashSet:

    def __init__(self, map_size=10000):
        """
        Initialize your data structure here.
        """
        self.map_size = map_size
        self.values = [None]*self.map_size

    def add(self, key: int) -> None:
        if self.contains(key): return

        hash_index = self.hash_function(key)

        if self.values[hash_index] is None:
            self.values[hash_index] = [key]
        else:
            self.values[hash_index].append(key)
        
    def remove(self, key: int) -> None:
        if self.contains(key):
            hash_index = self.hash_function(key)
            self.values[hash_index].remove(key)
        
    def contains(self, key: int) -> bool:
        hash_index = self.hash_function(key)
        if self.values[hash_index] is not None:
            return key in self.values[hash_index]
        return False
    
    def hash_function(self, key: int) -> int:
        return key % self.map_size

class HashElement:
    key: int
    value: int

    def __init__(self, key, value):
        self.key = key
        self.value = value
        
class HashMap:

    def __init__(self, map_size=10000):
        """
        Initialize your data structure here.
        """
        self.map_size = map_size
        self.values = [None]*self.map_size

    def put(self, key: int, value: int) -> None:
        hash_element = self.contains_key(key)
        # If it's already in the map, replace it with the new value
        # If it's not in the map, construct a new hash element and
        # insert it into the list of elements associated with the
        # given hash index
        if hash_element:
            hash_element.value = value
        else:
            hash_index = self.hash_function(key)
            if self.values[hash_index]:
                self.values[hash_index].append(HashElement(key, value))
            else:
                self.values[hash_index] = [HashElement(key, value)]
         
    def get(self, key: int) -> int:
        hash_element = self.contains_key(key)
        if hash_element:
            return hash_element.value
        else:
            return -1
    
    def contains_key(self, key: int) -> HashElement:
        hash_index = self.hash_function(key)
        hash_element_list = self.values[hash_index]
        if hash_element_list is None: return None
        for hash_element in hash_element_list:
            if key == hash_element.key:
                return hash_element
        return None
           
    def remove(self, key: int) -> None:
        # When using a Python list for the hash_element_list,
        # element removal is O(N) + O(N) .... O(N) asymptotically
        # Using a linked list for the hash_element_list
        # would allow for O(N) + O(1) removal
        hash_element = self.contains_key(key)
        if hash_element is None: return
        hash_index = self.hash_function(key)
        hash_element_list = self.values[hash_index]
        hash_element_list.remove(hash_element)

    def hash_function(self, key: int) -> int:
        return key % self.map_size
        


def contains_duplicate(list_ints):
    hash_set = set()
    for num in list_ints:
        if num in hash_set : return True
        hash_set.add(num)
    return False

def contains_single_number(list_ints):
    set_of_duplicates = set()
    hash_set = set()
    for num in list_ints:
        if num in hash_set : 
            set_of_duplicates.add(num)
        else:
            hash_set.add(num)
    difference = hash_set - set_of_duplicates
    return list(difference)[0]

