
<center>
<img src="docs/imgs/python.png" alt="Python"/>
</center>

# Welcome to the project
> This is Mike's starter Python project

I use this project as my starting point for any project I develop in Python.

## Getting started
We'll start by creating a workspace directory to hold code.

```shell
mkdir python_ws
cd python_ws
https://gitlab.com/mike-moore/python_starter_project.git
cd python_starter_project
```

The next step is setting up your Python environment. You have two options for this. You can use virtualenv or
Anaconda. [What's the difference?](https://docs.conda.io/projects/conda/en/latest/commands.html#conda-vs-pip-vs-virtualenv-commands)

### Python Environments
Create a Python 3 virtual environment called ".venv" in your python_starter_project folder. Virtual environments are 
generally a very good idea when developing your own Python modules and applications. I wish I 
would have known about these when I first started in Python. You can save yourself a lot of headache 
if you 
[read more about virtual environments](https://realpython.com/python-virtual-environments-a-primer/) 
before getting started with Python. The commands below will create and activate the needed environment 
for you. Environments have to be activated on a per shell basis.

```shell
virtualenv -p /usr/bin/python3.9 .venv
source .venv/bin/activate
```

Our next step is installing the Python modules needed for this course. The requirements.txt file in 
the env directory of this repository defines these dependencies. Defining a requirements.txt file is 
a best practice for Python development. The last step uses pip to install these packages 
into our newly created virtual environment. Pip is Python's package manager. You can [read more about 
pip here](https://realpython.com/what-is-pip/).

```shell
pip install -r env/requirements.txt
```


### Using Anaconda
Conda is an open-source package management system and environment management system that runs on Windows, macOS, and Linux. 
Conda quickly installs, runs, and updates packages and their dependencies. Conda easily creates, saves, loads, and switches 
between environments on your local computer. It was created for Python programs but it can package and distribute software for 
any language [[1]](https://docs.conda.io/projects/conda/en/latest/index.html).

Here is a [handy cheatsheet](https://docs.conda.io/projects/conda/en/4.6.0/_downloads/52a95608c49671267e40c689e0bc00ca/conda-cheatsheet.pdf) 
that I always refer to when working with Anaconda. Follow the steps below to create and activate a fresh environment for this starter project.

```shell
conda create --name py_practice_3.9 python=3.9
```


```shell
conda activate py_practice_3.9
```

```shell
pip install -r env/requirements.txt
```

### Automated Tests With Nosetest
Now that your environment is setup (using either venv or conda), you should be able to run this project's unit tests to confirm 
that everything is working. 

```shell
cd tests
nosetests
```

You should expect to see the following output:

```shell
.......
----------------------------------------------------------------------
Ran 6 tests in 0.005s

OK
```

This project uses the Python unittest module and nosetest for running our tests. You can 
learn more about test driven development with the 
<a href="https://www.freecodecamp.org/news/learning-to-test-with-python-997ace2d8abe/" target="_blank">Python unittest module here</a>.
You can also
<a href="http://pythontesting.net/framework/nose/nose-introduction/" target="_blank">read more about nose here</a>.

## Ready to Go
Once the unit-tests run successfully, you should be off to the races. You can start making your own modules and unit tests. 
You can also use Jupyter Notebook for prototyping and documentation.
