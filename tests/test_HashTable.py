from unittest import TestCase
import os, sys, logging, unittest
# Setup logger    
logging.basicConfig(level=logging.WARNING)
logging.addLevelName(logging.WARNING, "\033[93m%s\033[1;0m" % logging.getLevelName(logging.WARNING))
logging.addLevelName(logging.ERROR, "\033[91m%s\033[1;0m" % logging.getLevelName(logging.ERROR))
# Setup paths    
this_files_dir = os.path.abspath(os.path.dirname(__file__))
modules_dir = os.path.abspath(os.path.join(this_files_dir, os.pardir, 'src'))
sys.path.append(modules_dir)

class TestHashTable(TestCase):

    def test_hash_set_basic(self):
        from HashTable import HashSet
        my_hash_set = HashSet()
        my_hash_set.add(1) # set = [1]
        my_hash_set.add(2) # set = [1, 2]
        self.assertTrue(my_hash_set.contains(1))
        self.assertFalse(my_hash_set.contains(3))
        my_hash_set.add(2) # set = [1, 2]
        self.assertTrue(my_hash_set.contains(2))
        my_hash_set.remove(2) # set = [1]
        self.assertFalse(my_hash_set.contains(2))
        return

    def test_hash_map_basic(self):
        from HashTable import HashMap
        my_hash_map = HashMap()
        my_hash_map.put(1, 1) # The map is now [[1,1]]
        my_hash_map.put(2, 2) # The map is now [[1,1], [2,2]]
        self.assertEquals(1, my_hash_map.get(1)) # The map is now [[1,1], [2,2]]
        self.assertEquals(-1, my_hash_map.get(3)) # not found. The map is now [[1,1], [2,2]]
        my_hash_map.put(2, 1) # The map is now [[1,1], [2,1]] (i.e., update the existing value)
        self.assertEquals(1, my_hash_map.get(2)) # The map is now [[1,1], [2,1]]
        my_hash_map.remove(2) # remove the mapping for 2, The map is now [[1,1]]
        self.assertEquals(-1, my_hash_map.get(2)) # not found. The map is now [[1,1]]
        return
    
    def test_set_contains_duplicate(self):
        from HashTable import contains_duplicate
        nums = [1,2,3,1]
        self.assertTrue(contains_duplicate(nums))

    def test_set_contains_single_number(self):
        from HashTable import contains_single_number
        nums = [4,1,2,1,2]
        self.assertEquals(4, contains_single_number(nums))

if __name__ == '__main__':
    unittest.main()
